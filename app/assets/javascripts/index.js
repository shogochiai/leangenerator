var
Vue = require('vue'),
milkcocoa = new MilkCocoa("https://io-shwpxj0vx.mlkcca.com"),
ds = milkcocoa.DataStore("canvas");

ds.query({}).done(function (e) {
    for (var i = 0; i < e.length; i++) {
        renderCanvas(e[i]);
    }
});


ds.on("push", function (e) {
    renderCanvas(e.value);
});

var last_canvas = "dummy";

function renderCanvas(canvas) {
    var canvas_html = '<p class="post-text">' + escapeHTML(canvas.content) + '</p>';
    var date_html = '';
    if (canvas.date) {
        date_html = '<p class="post-date">' + escapeHTML(canvas.date.toLocaleString()) + '</p>';
    }
    $("#" + last_canvas).before('<div id="' + canvas.id + '" class="post">' + canvas_html + date_html + '</div>');
    last_canvas = canvas.id;
}

function post() {
    //5."canvas"データストアにメッセージをプッシュする
    ds.push({
        title: "タイトル",
        content: escapeHTML($("#content").val()),
        date: new Date()
    }, function (e) {
        $("#content").val("");
    });
}

$('#post').click(function () {
    post();
})
$('#content').keydown(function (e) {
    if (e.which == 13) {
        post();
    }
});

//インジェクション対策
function escapeHTML(val) {
    return $('<div>').text(val).html();
};

$('.header--signup').click(function(e) {
    $('#signup').lightbox_me({
        centered: true,
        onLoad: function() {
            $('#signup').find('input:first').focus()
        }
    });
    e.preventDefault();
});
