$('form').submit(function(e){
    return false
    //e.preventDefault();
});

$("#sign-up-submit").click(function(e) {
    var email = $("#sign-up [name=email]").val();
    var password = $("#sign-up [name=password]").val();
    var confirmation = $("#sign-up [name=password-confirmation]").val();

    if(password != confirmation) {
        alert('password間違えんなっつっただろ（おこ）');
        return false
    }

    milkcocoa.addAccount(email, password, null, function(error, user) {
        switch (error) {
        case null:
            console.log('正常に登録が完了しました');
            break;
        case 1:
            console.log('無効な書式のメールアドレスです');
            break;
        case 2:
            console.log('既に追加されているメールアドレスです');
            break;
        }
    });
});
