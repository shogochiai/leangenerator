var
html = 'app/views/',
js = 'app/assets/javascripts/',
css = 'app/assets/stylesheets/css/',
scss = 'app/assets/stylesheets/scss/';

module.exports = function (grunt) {
    var pkg = grunt.file.readJSON('package.json');

    grunt.initConfig({
        browserify : {  // タスク名. $ grunt browserify で実行できる
            dist : {
                src : js + 'index.js',  // エントリーポイントとなるファイル
                dest : js + 'application.js'  // 出力するファイル名
            }
        },
        sass : {
            dist : {
                files :  [{
                    expand : true,
                    cwd : scss,
                    src : ['*.scss'],
                    dest : css,
                    ext : '.css'
                }]
            }
        },

        watch : {  // タスク名. $ grunt watch で実行できる
            html_files: {
                files: html + '*.html' // ウォッチ対象として、ディレクトリ配下の*.htmlを指定
            },
            options: {
                livereload: true // 変更があればリロードするよ
            },
            scripts : {
                files : [
                    js + '*.js',
                    scss + '*.scss'
                ],  // 監視対象のファイル
                tasks : ['browserify', 'sass']  // 変更があったら呼ばれるタスク
            }
        }
    });

    // grunt関連のプラグインはpackage.jsonに記述されたものを読み込む
    Object.keys(pkg.devDependencies).forEach(function (devDependency) {
        if (devDependency.match(/^grunt\-/)) {
            grunt.loadNpmTasks(devDependency);
        }
    });

    // $ grunt で実行するタスクに watch を指定
    grunt.registerTask('default', ['watch']);
};
